#define _GNU_SOURCE

#include "rab.h"
#include <stdbool.h>
#include <ctype.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define grow(_p, _n, _a, _incr_amount) \
	if (_n >= _a) { \
		_a += _incr_amount; \
		_p = realloc (_p, _a * sizeof (typeof (*_p))); \
		assert (_p); \
	}
#define add(_p, _n, _a, _incr_amount, ...) ({ \
	grow (_p, _n, _a, _incr_amount); \
	(_p)[(_n)++] = (typeof (*(_p))) __VA_ARGS__; \
	&(_p)[(_n) - 1]; \
	})
#define max(a,b) 																		\
	({ __typeof__ (a) _a = (a); 														\
	 __typeof__ (b) _b = (b); 															\
	 _a > b? _a : _b; })
#define each(_item, _n, _arr) \
	for (__typeof__ (_arr[0]) *_item = _arr; _item < _arr + _n; _item++)
#define fori(_idx, _max) 																\
	for (__typeof__ (_max) _idx = 0; _idx < _max; _idx++)
#define nuke_ptr(_ptr) do { free (_ptr); _ptr = 0; } while (0)
#if 0
/**
 * Locate a character in string, stopping after n chars
 *
 * It's just like strchr, except it stops looking after n chars
 * compared.
 *
 * Note that c is a char, not an int.
 *
 * @param s The string to be searched.
 * @param c The character to look for.
 * @param n Max number of chars to search.
 * @return A pointer to the the character if found, else NULL.
 */
char *rab_strnchr (const char *s, char c, size_t n) {
	const char *p;
	for (p = s; *p && p - s < n; p++)
		if (*p == c) return (char *) p;
	return 0;
}
#endif

static ssize_t get_n_objs_from_sentinel (void *objs, size_t size, void *sentinel) {
	int r;
	for (r = 0;; r++) {
		unsigned char *elem = objs + (r * size);
		int var = memcmp (elem, sentinel, size);
		if (!var)
			break;
	}
	return r;
}

// This is for those functions (I think all will be like this
// eventually) which operate on some user void * array. We want
// version of those functions which don't require you to pass a
// member-getting function. Those functions will call the more
// long-winded versions with this function (which just returns its)
// argument.
static inline char **get_str_member (const void *user_type, void *) {
	return (char **) user_type;
}

struct rab_getter rab_default_getter = {
	.func = get_str_member,

	// I have a feeling this is a bad idea.
	.type_size = sizeof (char *),
};

static int rab_strn_cmp (const void *a, const void *b, int len, void *) {
	return strncmp (a, b, len);
}

static int rab_str_cmp (const void *a, const void *b, int, void *) {
	return strcmp (a, b);
}
/**
 * search a string for any of a set of bytes, stopping at n chars
 *
 * It's just like strpbrk,
 * it stops looking after n chars compared.
 *
 * @param s The string to be searched.
 * @param accept The characters to look for. Must be null-terminated.
 * @param n Max number of chars to search.
 * @return A pointer to the the character if found, else NULL.
 */
char *rab_strnpbrk (const char *s, const char *accept, size_t n) {
	const char *p;
	for (p = s; *p && (size_t) (p - s) <  n; p++) {
		for (const char *q = accept; *q; q++)
			if (*q == *p) return (char *) p;
	}
	return 0;
}

char *rab_strpbrknul (const char *s, const char *accept) {
	char *r = strpbrk (s, accept);
	if (!r)
		r = strchr (s, 0);
	return r;
}

char *rab_strnpbrknul (const char *s, const char *accept, size_t n) {
	char *r = rab_strnpbrk (s, accept, n);
	if (!r)

		// I'm aware that strchrnul doesn't take an n, and so
		// rab_strnpbrknul could return a value > s + n. However,
		// that's just the way it is. I could have it return 0 if you
		// go behond s + n, but I think this is better. You can test
		// it after, see how far you went, which might be useful.
		// Having said that, this function isn't tested.
		r = strchrnul (s, 0);
	return r;
}

char *rab_strrstr (const char *haystack, const char *needle) {
	int n_needle = strlen (needle);
	const char *r = haystack + strlen (haystack) - n_needle;
	if (r < haystack)
		return 0;

	while ((r = strrchr (haystack, *needle))) {
		if (strncmp (r, needle, n_needle))
			return (char *) r;
	}
	return 0;
}

char *rab_strstrs (const char *haystack, const char **needles, size_t n_needles) {

	char *r = 0;

	// FIXME: This is probably a pretty inefficient way of doing
	// it.
	each (_, n_needles, needles) {
		if ((r = strstr (haystack, *_)))
			return  r;
	}
	return 0;
}

// I can't find the words. Here's an example of what this does:
// 		strs = {"this", "that", "these"}
// 			returns 2 because "th" is at the start of all three.
// 		strs = {"fuck", "that", "these"} returns 0
// 			returns 0 because the strings don't all share initial
// 			chars.
int rab_n_shared_initial_chars (char **strs, int n_strs) {
	int r = 0;

	// Going through character-by-character, not word-by-word.
	while (r++) {
		char **str = strs;
		char this_char = (*str)[r];

		// Now going through each string.
		for (; str < strs + n_strs; str++)
			if ((*str)[r] != this_char) goto out;
	}
out:;
	return r;
}

// This function is called index_of because I realised it can be quite
// generic, since you pass the getter in and the cmp func. However,
// really such a function should take a void pointer, not a pattern.
int rab_index_of (size_t n_objs, void *objs, char *pattern, ssize_t n_pattern,
		struct rab_getter *getter, rab_cmp_func cmp_func) {
	if (!getter)
		getter = &rab_default_getter;
	if (!cmp_func) {
		if (n_pattern == -1)
			cmp_func = rab_str_cmp;
		else
			cmp_func = rab_strn_cmp;
	}
	fori (i, n_objs) {
		char **str = getter->func (objs + (i * getter->type_size), getter->user);
		if (!cmp_func (*str, pattern, n_pattern, 0))
			return i;
	}
	return -1;
}

int rab_indexes_of (int *pr, size_t n_objs,
		void *objs, size_t n_pattern, char *pattern, struct rab_getter *getter,
		rab_cmp_func cmp_func) {

	if (!getter)
		getter = &rab_default_getter;
	cmp_func = cmp_func ?: rab_strn_cmp;
	int r = 0;
	fori (i, n_objs) {
		char **str = getter->func (objs + (i * getter->type_size), getter->user);
		if (!cmp_func (pattern, *str, n_pattern, 0))
			pr[r++] = i;
	}
	return r;
}

int rab_get_submatching_strings (int *pr, size_t n_objs,
		void *objs, char *match, struct rab_getter *getter) {

	if (!getter)
		getter = &rab_default_getter;

	int r = 0;
	fori (i, n_objs) {
		char **str = getter->func (objs + (i * getter->type_size),
				getter->user);
		if (strstr (*str, match))
			pr[r++] = i;
	}
	return r;
}

int rab_get_first_str_with_submatch (size_t n_objs,
		void *objs, char *match, struct rab_getter *getter) {

	if (!getter)
		getter = &rab_default_getter;

	int r = -1;
	fori (i, n_objs) {
		char **str = getter->func (objs + (i * getter->type_size),
				getter->user);
		if (strstr (*str, match)) {
			r = i;
			break;
		}

	}
	return r;
}

char *rab_to_x (char *str, int (*ctype_func) (int)) {

	char *r;
	for (r = str; *r; r++) {
		if (ctype_func (*r))
			break;
	}
	return r;
}

char *rab_to_x_within (char *str, size_t n, int (*ctype_func) (int)) {

	char *r;
	for (r = str; *r && r < str + n; r++) {
		if (ctype_func (*r))
			break;
	}
	return r;
}

char *rab_skip_x (char *str, int (*ctype_func) (int)) {

	char *r;
	for (r = str; *r; r++) {
		if (!ctype_func (*r))
			break;
	}
	return r;
}

char *rab_skip_x_within (char *str, size_t n, int (*ctype_func) (int)) {

	char *r;
	for (r = str; *r && r < str + n; r++) {
		if (!ctype_func (*r))
			break;
	}
	return r;
}

#if 0
char *rab_get_first_non_locale_ctype (char *str, size_t n,
		int (*ctype_func) (int, locale_t locale), locale_t locale) {

	char *r;
	for (r = str; r < str + n; r++) {
		if (*r && !ctype_func (*r, locale))
			break;
	}
	return r;

}
#endif

bool rab_is_all_x (char *str, int (*ctype_func) (int)) {
	char *rt = rab_skip_x (str, ctype_func);
	if (!*rt)
		return 1;
	return 0;
}

bool rab_is_all_x_within (char *str, ssize_t n, int (*ctype_func) (int)) {
	return rab_skip_x_within (str, n, ctype_func) - str == n;
}

#if 0
bool rab_is_all_locale_x (char *str, size_t n, int (*ctype_func) (int, locale_t locale),
		locale_t locale) {
	return rab_get_first_non_locale_ctype (str, n, ctype_func, locale) - str == n;
}
#endif

// Hard to explain, as usual.
// Example:
// 		strs = {"this", "that", "hi"}
// 			returns {"thi", "tha", "h"}.
// See?
char **rab_get_unique_prefixes (size_t n_strs, char **strs,
		size_t n_error_buf, char error_buf[n_error_buf]) {
	int n_r = 0;
	char **r = malloc (sizeof *r * n_strs);
	each (s, n_strs, strs) {
		bool matching[n_strs];
		fori (i, n_strs)
			matching[i] = 1;
		int n_matching = n_strs;
		bool done = 0;
		int i;
		if (!**s) {
			snprintf (error_buf, n_error_buf, "str %zu is empty", s - strs);
			goto fail;
		}
		for (i = 0;; i++) {
			if (!(*s)[i]) {

				// Set "done" here because I use it to figure out
				// if you've passed an empty string.
				done = 1;
				break;
			}
			for (char **t = strs; t < strs + n_strs; t++) {

				// We don't do anything when t == s; this means that
				// matches[t - s] will always be true, and that
				// when we compare stuff to n_matching, we
				// compare it to 1. Because there will always be
				// one that's true.
				if (t == s || !matching[t - strs])
					continue;
				if ((*t)[i] == 0 || (*s)[i] != (*t)[i]) {
					matching[t - strs] = 0;
					n_matching--;
					if (n_matching == 1) {
						done = 1;
						break;
					}
				}
			}
			if (done)
				break;
		}
		if (done && n_matching > 1) {
			snprintf (error_buf, n_error_buf, "\"%s\" can't be made unique", *s);
			goto fail;
		}

		r[n_r++] = strndup (*s, i + 1);
	}
	return r;
fail:
	fori (i, n_r)
		free (r[i]);
	free (r);
	return 0;
}

int rab_n_matching (char *a, char *b) {
	int r = 0;
	for (int i = 0; a[i]; i++) {
		if (a[i] == b[i]) r++;
		else              break;
	}
	return r;
}

// Gets the sort of thing you'd expect to get when you tab-complete
// something, and when you type "pri" and assume it will resolve to the
// an unabiguous match, "print".
// Example:
// 		query = "th", strs = {"this", "that", "hi"} returns {"this",
// 				"that"}
// 		query = "hi", strs = {"this", "that", "hi"} returns {"hi"}
// You need to pass it a buffer big enough, which is to say n_strs
// big unless you want fewer. You ask for fewer by setting n_pr to the
// value beforehand.
int rab_get_completion (char *query, size_t n_objs, const void *objs,
		rab_getter *getter, size_t max_completions_wanted,
		rab_completion_matches *out_matches) {

	assert (max_completions_wanted > 0);

	if (!getter)
		getter = &rab_default_getter;

	// We could do nothing here and let it crash.
	if (!objs)
		return 1;

	if (!query ||

			// Technically, I say, a query of "" should match
			// everything. And that could be useful for you, could
			// save a few lines of code. But. We'll return 0 for now.
			!*query)
		return 1;

	int query_len = strlen (query);

	fori (i, n_objs) {

		const void *obj = objs + (i * getter->type_size);
		char **str = getter->func (obj, getter->user);

		// Just ignore null and empty objs. We could optionally error
		// out on null ones. No, it's not a library function's job to
		// check stuff like that. 2022-11-15T04:20:09+00:00: Yeah, i
		// know it's kind of embarrassing to check three times like
		// this. In truth, This function doesn't need a str-getting
		// function that return a double pointer, so we could save a
		// check here, but I have this notion a lot of this library
		// will str-getting functions, and if that's the case I think
		// it'd be nice to only ever make one of those. Simpler.
		if (!str || !*str || !**str) continue;

		if (memcmp (*str, query, query_len))
			continue;

		int str_len = strlen (*str);

		// A full match; we exit. This means that if your query is
		// "cheese", and there's "cheese" and "cheese-monster", you'll
		// match "cheese". Make this an option if you want.
		if (str_len == query_len) {
			out_matches->count = 1;
			*out_matches->idxes = i;
			out_matches->len = str_len;
			return 0;
		}
		int n_initial_matching = rab_n_matching (*str, query);
		if (!n_initial_matching)
			continue;
		if (n_initial_matching > out_matches->len) {
			out_matches->count = 1;
			out_matches->idxes[0] = i;
			out_matches->len = n_initial_matching;
		} else if (n_initial_matching == out_matches->len)
			out_matches->idxes[(out_matches->count)++] = i;
	}

	/* if (max_wanted < *n_pr_and_max_wanted_watch_out) */
	/* 	*n_pr_and_max_wanted_watch_out = max_wanted; */

	return 0;
}

int rab_print_comma_list (

		// Ignored if rab_pcl_opts.sentinel is set. See notes in
		// rab_pcl_opts about that.
		ssize_t n_objs,
		void *objs, struct rab_pcl_opts *opts) {
	int r = 0;

	struct rab_pcl_opts default_opts = {
		.and_str = "and",
		.getter = &rab_default_getter,
		.f = stdout,
		.flags = 0,
		.type = RAB_CT_OXFORD,
	};

	if (!opts)
		opts = &default_opts;
	if (!opts->and_str)
		opts->and_str = default_opts.and_str;
	if (!opts->getter)
		opts->getter = default_opts.getter;

	size_t size = opts->getter->type_size;
	if (opts->getter->sentinel)
		n_objs = get_n_objs_from_sentinel (objs, size, opts->getter->sentinel);

	// Let's use this macro trickery to make quoting the strings
	// neater. I could copy the string to a buffer, of course,
	// instead. Might be faster if lots of strings?
#define $qc() \
	(opts->flags & RAB_PCLF_DONT_QUOTE_WORDS ? "" : "\"")
#define $quoted_str_args \
	$qc (), str_member, $qc ()
#define $quoted_str_fmt "%s%s%s"
	for (int i = 0; i < n_objs; i++) {
		char **str_member_p = opts->getter->func (objs + (i * size), opts->getter->user);
		assert (str_member_p && *str_member_p);
		char *str_member = *str_member_p;
		if (i == 0)
			fprintf (opts->f, $quoted_str_fmt, $quoted_str_args);

		else if (i == n_objs - 1) {

			switch (opts->type) {
				case RAB_CT_NONE: fprintf (opts->f, ", %s", str_member); break;
				case RAB_CT_OXFORD:
					fprintf (opts->f, "%s %s " $quoted_str_fmt,
						i != 1 ||
						opts->flags & RAB_PCLF_COMMA_EVEN_WITH_TWO_ENTRIES ? "," : "",
							opts->and_str, $quoted_str_args);
					break;
				case RAB_CT_NON_OXFORD:
					fprintf (opts->f, "%s " $quoted_str_fmt, opts->and_str,
							$quoted_str_args);
					break;
			}
		} else
			fprintf (opts->f, ", " $quoted_str_fmt, $quoted_str_args);
	}

	if (opts->end)
		fputs (opts->end, opts->f);
	return r;
}

char *rab_get_comma_list (size_t n_objs, void *objs, struct rab_pcl_opts *_opts) {
	struct rab_pcl_opts *opts = _opts ?: &(struct rab_pcl_opts) {};
	char *r; size_t n_r;
	FILE *f = open_memstream (&r, &n_r);
	if (!f) return 0;

	// We silently ignore the user's _opts->f. Obviously they
	// shouldn't pass it since they're getting a string. But they
	// might want to use opts, so best to allow it.
	opts->f = f;

	int rc = rab_print_comma_list (n_objs, objs, opts);
	if (rc) {
		fclose (f);
		return 0;
	}
	fclose (f);
	return r;
}

char *rab_get_str_ap (char *fmt, va_list ap) {
	char *r = 0;
	int rt = vasprintf (&r, fmt, ap);
	assert (rt != -1);
	return r;
}

// Gets a str. Right now, it just exits on failure. Obviously this is
// bad behaviour for a library. But -- come on.
char *rab_get_str (char *fmt, ...) {
	va_list ap; va_start (ap, fmt);
	char *r = rab_get_str_ap (fmt, ap);
	va_end (ap);
	return r;
}

// See rab_make_strs_unique. This takes an array of whatever type.
// get_str_func should be like this:
//
// 		char **get_member (void *user) {
// 			struct whatever *whatever = user;
// 			return &whatever->the_member_in_question;
// 		}
void rab_make_strs_unique (size_t n_user_arr, void *user_arr,
		bool free_dups, struct rab_getter *getter) {

	if (!getter)
		getter = &rab_default_getter;
	fori (i, n_user_arr) {
		int n_matches = 0;
		char **str_i = getter->func (user_arr + (i * getter->type_size),
				getter->user);
		for (size_t j = i; j < n_user_arr; j++) {
			char **str_j = getter->func (user_arr + (j * getter->type_size),
					getter->user);
			if (i != j && !strcmp (*str_i, *str_j)) {
				char *tmp = 0;
				asprintf (&tmp, "%s-%d", *str_j, n_matches + 1);

				if (free_dups) free (*str_j);
				*str_j = tmp;
				n_matches++;
			}
		}
	}
}

// Note (2023-01-29T01:50:46): Probably this function shouldn't be
// here. It's not especially to do with strings. But what are strings?
// I have a feeling this library might become just a grab-bag, maybe
// with a focus on generic functions. This function's adapted from
// something found on the internet.
int rab_remove_dups (size_t n_arr, size_t elem_size, void *arr,
		int (*cmp_func) (const void *, const void *),
		void (*free_func) (void *)) {

    for (size_t i = 0; i < n_arr; i++) {

        for (size_t j = i + 1; j < n_arr; j++) {

			 void *elem_i = arr + (i * elem_size);
			 void *elem_j = arr + (j * elem_size);

            if (!cmp_func (elem_i, elem_j)) {

				if (free_func)
					free_func (elem_j);

				memcpy (elem_j, elem_j + elem_size, (arr + (n_arr * elem_size)) -
						(elem_j + elem_size));

                n_arr--;

                j--;
            }
        }
    }
	return n_arr;
}

// Return the result of make_strs_unique.
char **rab_get_uniqued_strs (size_t n_strs, char **strs, bool free_dups) {

	char **r = malloc (sizeof *r * n_strs);
	for (size_t i = 0; i < n_strs; i++)
		r[i] = strdup (strs[i]);
	rab_make_strs_unique (n_strs, r, free_dups, 0);
	return r;
}

// It's just a join function, except it operates on arrays of objs and
// takes a "getter". It ignores null and empty strings.
int rab_print_joined (int n_objs, void *objs, struct rab_join_opts *opts) {

	FILE *f = opts && opts->f ? opts->f : stdout;
	struct rab_getter *getter = opts && opts->getter
		? opts->getter
		: &rab_default_getter;
	char *joining_chars = opts && opts->glue ? opts->glue : " ";
	char *quote_chars = opts && opts->quotes ? opts->quotes : "";
	char *end_str = opts && opts->end ? opts->end : 0;

	for (int i = 0; i < n_objs; i++) {
		char **str = getter->func (objs + (i * getter->type_size),
				getter->user);

		// This is pretty dumb, right, but it's got to be done.
		// Maybe later we'll have a KEEP_EMPTY flag, which would do
		// whatever other join programs and commands do when they
		// encounter empty elements. I suppose they would print the
		// delim, which might be a space. If you were using the quote
		// chars you would print those, too.
		if (!str || !*str || !**str)
			continue;

		fputs (quote_chars, f);
		// A wild guess: generally you're joining small strings. Seems
		// dumb to strlen and fwrite. So we just go char by char.
		for (char *p = *str; *p; p++)
			fputc (*p, f);
		fputs (quote_chars, f);
		if (i != n_objs - 1)
			fputs (joining_chars, f);
	}
	if (end_str)
		fputs (end_str, f);
	return 0;
}

// See rab_print_joined
char *rab_join (int n_objs, void *objs, struct rab_join_opts *_opts) {
	char *r = 0; size_t n_r = 0;
	FILE *f = open_memstream (&r, &n_r);
	struct rab_join_opts opts = {};
	if (_opts)
		opts = *_opts;
	opts.f = f;
	rab_print_joined (n_objs, objs, &opts);
	fclose (f);
	return r;
}

void rab_strip (char *s) {
	char *p = s;
	int len = strlen (p);
	if (len == 0)
		return;

	while (isspace (p[len - 1]))
		p[--len] = 0;

	while (*p && isspace (*p))
		++p, --len;

	memmove (s, p, len + 1);

}

struct rab_memstream rab_get_memstream (char *fmt, ...) {
	struct rab_memstream r = {};
	r.f = open_memstream (&r.buf, &r.n_buf);
	if (r.f && fmt) {
		va_list ap;
		va_start (ap, fmt);
		vfprintf (r.f, fmt, ap);
		va_end (ap);
	}
	return r;
}

struct rab_memstream rab_get_memstream_ap (char *fmt, va_list ap) {
	struct rab_memstream r = {};
	r.f = open_memstream (&r.buf, &r.n_buf);
	if (r.f && fmt)
		vfprintf (r.f, fmt, ap);
	return r;
}

int rab_just_write_to_memstream (struct rab_memstream *memstream, char *fmt, ...) {

	va_list ap;
	va_start (ap, fmt);
	int r = 1;
	if (!memstream->f) {
		memstream->f = open_memstream (&memstream->buf, &memstream->n_buf);
		if (!memstream->f)
			goto out;
	}
	r = vfprintf (memstream->f, fmt, ap);
out:
	va_end (ap);
	return r;
}

int rab_maybe_close_memstream (struct rab_memstream *memstream, bool free_it) {
	if (!memstream->f)
		return 0;
	int r = fclose (memstream->f);
	if (r)
		return r;
	if (free_it && memstream->buf) {
		free (memstream->buf);
		*memstream = (typeof (*memstream)) {};
	}
	return 0;
}


// Honestly, this is not a bad function. It surrounds text with other
// text. "this" with "left" and "right" or "(" and ")" would yield
// "(this)". I wrote this to make printing colours with printf easier.
int rab_bookend (ssize_t n_pr, char *pr, char *fmt, char *left, char *right) {

	ssize_t orig_len = strlen (pr);
	char orig[orig_len + 1];
	strcpy (orig, pr);

	char *_ = pr;
	_ = stpcpy (_, left);
	_ += sprintf (_, fmt, orig);
	_ = stpcpy (_, right);
	*_++ = 0;

	assert (_ - pr < n_pr);
	return 0;
}

// I write this function over and over. I won't combine it with, say
// rab_get_submatching_strings, because then you'd have to write a
// version with rab_indexes_of_n_matching_strings, etc. We'll call it
// rab_have_just_one_match. That doesn't sum up the fact it prints
// error messages. In fact, printing error messages is the main thing
// it does. But this is fine for now.
bool rab_have_just_one_match (int n_matches, size_t n_objs, void *objs, char *query,

		// Eg "files": "doesn't match any files".
		char *whats, struct rab_getter *getter) {
	if (!getter)
		getter = &rab_default_getter;
	int r = 0;
	switch (n_matches) {
		case 1: r = 1; break;
		case 0: fprintf (stderr, "%s: \"%s\" doesn't match any %s\n",
						program_invocation_short_name, query, whats);
			break;
		default: {
			fprintf (stderr, "%s: Too many matches for \"%s\"; ",
					program_invocation_short_name, query);
			char *names[n_matches];
			fori (i, n_objs) {
				char **str = getter->func (objs + (i * getter->type_size),
						getter->user);
				names[i] = *str;
			}
			rab_print_comma_list (n_matches, names,
					&(struct rab_pcl_opts) {.f = stderr, .end = "\n"});
		}
	}
	return r;
}

int rab_get_longest_line (char *s) {
	int r = 0, this_len = 0;

	for (;;) {
		if (!*s || *s == '\n') {
			r = max (r, this_len);
			this_len = 0;
		} else {
			this_len++;
		}
		if (!*s)
			break;
		s++;
	}
	return r;

}

bool rab_char_in_str_is_escaped (char *str, int pos) {
	int j = pos - 1;
	int n_backslashes = 0;

	// Count preceding backslashes.
	while (j >= 0 && str[j] == '\\') {
		++n_backslashes;
		--j;
	}

	// If there's an odd number of backslashes it's escaped.
	if (n_backslashes % 2)
		return 1;

	return 0;
}

// This is a basic "shell split", that is it turns a string like
//
// 	"this that" the other
//
// into (char *[]) {"this that", "the", "other"}
//
// Unlike wordexp it doesn't fail on shell characters like "(".
char **rab_shell_split (char *str, ssize_t *n_r) {

	*n_r = 0;
	char *s = str;

	int a = 0;
	int incr = 16;
	char **r = NULL;
	while (isspace (*s))
		s++;
	if (!*s)
		return r;

	char word_buf[256];

	while (*s) {

		memset (word_buf, 0, sizeof word_buf);
		char *word_buf_p = word_buf;
		char *p = s;
		while (*p && !isspace (*p)) {
			if ((*p == '"' || *p == '\'') && !rab_char_in_str_is_escaped (str, p - str)) {
				char quote_char = *p;
				p++;
				while (*p && !(*p == quote_char &&
						!rab_char_in_str_is_escaped (str, p - str))) {
					*word_buf_p++ = *p++;
				}
				break;
			}
			*word_buf_p++ = *p++;
		}
		add (r, *n_r, a, incr, strndup (word_buf,
					word_buf_p - word_buf));

		if (!*p)
			break;
		s = p + 1;
		while (isspace (*s))
			s++;
	}

	return r;
}

char *rab_find_unescaped_ch (char *s, char *e, char ch) {
	if (!e)
		e = strchr (s, 0);
	char *r = s;
	while (r != e) {

		if (*r == ch && !rab_char_in_str_is_escaped (s, r - s))
			break;
		r++;
	}
	return r;
}

// Destructive. Doesn't strdup each str, ie, you need to keep the
// strings around.
char **rab_strsep_str (int *n_r, char *s, char *delims) {
	char *token;

	*n_r = 0;
	int a_r = 16;
	char **r = malloc (sizeof *r * a_r);
	while ((token = strsep (&s, delims))) {
		if (*n_r >= a_r) {
			a_r *= 2;
			r = reallocarray (r, a_r, sizeof *r);
		}
		r[(*n_r)++] = token;
	}
	return r;
}

// Doesn't allocate because presumably you're printing
// or fprinting
char *rab_get_num_suffix (size_t _num) {
	long long num = _num;
	while (num > 20) num %= 10;
	switch (num) {
		case 0:
			return "th";
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		case 4: // fall-through
		case 5: // fall-through
		case 6: // fall-through
		case 7: // fall-through
		case 8: // fall-through
		case 9: // fall-through
		case 10: // fall-through
		case 11: // fall-through
		case 12: // fall-through
		case 13: // fall-through
		case 14: // fall-through
		case 15: // fall-through
		case 16: // fall-through
		case 17: // fall-through
		case 18: // fall-through
		case 19: // fall-through
		case 20:
			return "th";
		default:
			assert (0);
			break;
	}
	assert (0);
	return 0;
}


char *rab_wordexp_strerror (int rc) {
	switch (rc) {
		case WRDE_BADCHAR:
	      return "Illegal occurrence of newline or one of |, &, ;, <, >, (, ), {, }.";
		case WRDE_BADVAL:
	      return "\
An undefined shell variable was referenced, and the WRDE_UNDEF flag told us to consider \
this an error";
		case WRDE_CMDSUB:
		  return "\
Command substitution requested, but the WRDE_NOCMD flag told us to consider this an \
error";
		case WRDE_NOSPACE:
	      return "Out of memory";
		case WRDE_SYNTAX:
	      return "Shell syntax error, such as unbalanced parentheses or unmatched quotes";
		default:
		  assert (!"You've passed a wordexp return code that doesn't mean anything");
	}

	return 0;
}

rab_slice rab_scan_preg (char *s, regex_t preg) {

	char *p = s;
	regmatch_t match;

	// We pass REG_NOTBOL | REG_NOTEOL because we're parsing the
	// string in different places. We might want to pass
	// REG_NOTBOL only after the first, and REG_NOTEOL for the
	// last, but how do you know when it's the last? And what
	// happens if your pattern has ^ and $ in it and matches the
	// whole string in the first go? Basically I'm confused. I
	// don't think that passing ^ and $ serves much purpose in a
	// function like this anyway.
	int rc = regexec (&preg, p, 1, &match, REG_NOTBOL | REG_NOTEOL);
	if (rc)
		return (rab_slice) {};

	return (rab_slice) {.s = p + match.rm_so, .e = p + match.rm_eo};
}

// Returns a slice where the pattern matches. It's really a wrapper
// around regexec. Use rab_scan_preg if you're using the same pattern
// over and over.
rab_slice rab_scan (char *s, char *pattern) {

	regex_t preg;
	int rc = regcomp (&preg, pattern, REG_EXTENDED);
	if (rc)
		return (rab_slice) {};

	return rab_scan_preg (s, preg);
}

// Straight from ChatGPT. Except I added the g flag, which does what
// you'd expect.
char* rab_subst (const char* str, const char* from, const char* to, bool g) {
    // Check for NULL pointers
    if (!str || !from || !to)
        return NULL;

    // Calculate lengths
    size_t orig_len = strlen (str);
    size_t target_len = strlen (from);
    size_t replacement_len = strlen (to);

    // Count occurrences of from in str
    size_t count = 0;
    const char *temp = str;
    while ((temp = strstr (temp, from)) != NULL) {
        count++;
        temp += target_len; // Move past the last found occurrence
    }

    // If no occurrences, return a copy of the str string
    if (count == 0) {
        char* result = malloc (orig_len + 1);
        if (result) {
            strcpy (result, str);
        }
        return result;
    }

    // Calculate the new length for the result string
    size_t new_length = orig_len + count * (replacement_len - target_len);
    char* result = malloc (new_length + 1); // +1 for the null terminator
    if (!result) {
        return NULL; // Memory allocation failed
    }

    // Perform the substitution
    char* current_pos = result;
    const char* orig_pos = str;

    while ((temp = strstr (orig_pos, from)) != NULL) {
        // Copy the part before the from
        size_t bytes_to_copy = temp - orig_pos;
        strncpy (current_pos, orig_pos, bytes_to_copy);
        current_pos += bytes_to_copy;

        // Copy the to
        strcpy (current_pos, to);
        current_pos += replacement_len;

        // Move past the from in the str string
        orig_pos = temp + target_len;
		if (!g)
			break;
    }

    // Copy any remaining part of the str string
    strcpy (current_pos, orig_pos);

    return result;
}


#undef $each
#undef fori
