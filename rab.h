#pragma once
#include <stdbool.h>
#include <wordexp.h>
#include <regex.h>
#include <stddef.h>
#include <stdio.h>
#include <unistd.h>

#define rab_skip_space(_p) rab_skip_x (_p, isspace)
#define rab_skip_blank(_p) rab_skip_x (_p, isblank)
#define rab_to_space(_p) rab_to_x (_p, isspace)
#define rab_to_blank(_p) rab_to_x (_p, isblank)

enum rab_comma_type {
	RAB_CT_OXFORD,
	RAB_CT_NON_OXFORD,
	RAB_CT_NONE,
};

enum rab_pcl_flags {
	RAB_PCLF_COMMA_EVEN_WITH_TWO_ENTRIES = 1 << 0,
	RAB_PCLF_DONT_QUOTE_WORDS = 1 << 1,
};

typedef struct rab_completion_matches rab_completion_matches;
struct rab_completion_matches {

	int count;

	// You're expected to allocate this. You know the max number of
	// matches because it can't be more than the number of strings you
	// pass in. But you also don't need to malloc it. You can declare
	// a static array and point this to that.
	int *idxes;

	// The number of chars that match, not the length of the matching
	// str. It will be the same for all matches.
	int len;
};


typedef struct rab_slice rab_slice;
struct rab_slice {
	char *s, *e;
};

// The names of these members are short because this function is meant
// to be convenient. It becomes less convenient when it clutters up
// the page.
struct rab_pcl_opts {
	char *and_str;
	struct rab_getter *getter;
	FILE *f;
	enum rab_pcl_flags flags;
	enum rab_comma_type type;
	char *end;
};
typedef struct rab_pcl_opts rab_pcl_opts;
typedef struct rab_pcl_opts rab_pclo;

struct rab_join_opts {
	FILE *f;
	char *glue;
	char *quotes;
	char *end;
	struct rab_getter *getter;
};
typedef struct rab_join_opts rab_join_opts;
typedef struct rab_join_opts rab_join_o;

// This returns a double pointer because there's at least one function
// that modifies the string.
#define MAKE_RAB_GETTER_FUNC(name) char **name (const void *ptr, void *user)
typedef MAKE_RAB_GETTER_FUNC (rab_getter_func);

#define MAKE_RAB_CMP_FUNC(name)int name (const void *a, const void *b, int len, void * user)
typedef MAKE_RAB_CMP_FUNC (rab_cmp_func);

struct rab_getter {
	rab_getter_func *func;
	size_t type_size;
	void *user;

	// Set this if your array uses a sentinel value. You can't leave
	// its as 0 if your sentinel is zero, else the lib can't tell if
	// apart from "not give"n. Do soemthing like this:
	// 		.sentinel = (int []) {0}.
	void *sentinel;
};
typedef struct rab_getter rab_getter;
extern struct rab_getter rab_default_getter;

struct rab_memstream {
	FILE *f; char *buf; size_t n_buf;
};

/* char *rab_strnchr (const char *s, char c, size_t n); */
char *rab_strnpbrk (const char *s, const char *accept, size_t n);
char *rab_strpbrknul (const char *s, const char *accept);
char *rab_strnpbrknul (const char *s, const char *accept, size_t n);
char *rab_strrstr (const char *haystack, const char *needle);
int rab_n_shared_initial_chars (char **strs, int n_strs);
int rab_index_of (size_t n_objs, void *objs,
		char *pattern, ssize_t n_pattern, struct rab_getter *str_getter,
		rab_cmp_func cmp_func);
int rab_indexes_of (int *pr, size_t n_objs,
		void *objs, size_t n_pattern, char *pattern, struct rab_getter *str_getter,
		rab_cmp_func cmp_func);
int rab_get_submatching_strings (int *pr, size_t n_objs,
		void *objs, char *match, struct rab_getter *getter);
int rab_get_first_str_with_submatch (size_t n_objs,
		void *objs, char *match, struct rab_getter *getter);
char *rab_skip_x (char *str, int (*ctype_func) (int));
bool rab_is_all_x (char *str,  int (*ctype_func) (int));
char *rab_to_x (char *str, int (*ctype_func) (int));
char *rab_to_x_within (char *str, size_t n, int (*ctype_func) (int));
char *rab_skip_x_within (char *str, size_t n, int (*ctype_func) (int));
#if 0
char *rab_get_first_non_locale_ctype (char *str, size_t n,
		int (*ctype_func) (int, locale_t locale), locale_t locale);
#endif
bool rab_is_all_x_within (char *str, ssize_t n, int (*ctype_func) (int));
#if 0
bool rab_is_all_locale_x (char *str, size_t n, int (*ctype_func) (int, locale_t locale),
		locale_t locale);
#endif
char *rab_strstrs (const char *haystack, const char **needles, size_t n_needles);
char **rab_get_unique_prefixes (size_t n_strs, char **strs,
		size_t n_error_buf, char error_buf[n_error_buf]);
int rab_print_comma_list (ssize_t n_objs, void *objs, struct rab_pcl_opts *opts);
int rab_print_reg_comma_list (FILE *f, ssize_t n_objs, void *objs,
		struct rab_getter *getter, char *end_string);
char *rab_get_comma_list (size_t n_objs, void *objs, struct rab_pcl_opts *_opts);
char *rab_get_str (char *fmt, ...);
char *rab_get_str_ap (char *fmt, va_list ap);
int rab_n_matching (char *a, char *b);
int rab_get_completion (char *query, size_t n_objs, const void *objs,
		rab_getter *getter, size_t max_completions_wanted,
		rab_completion_matches *out_matches);
char **rab_get_uniqued_strs (size_t n_strs, char **strs, bool free_dups);
void rab_make_strs_unique (size_t n_user_arr, void *user_arr,
		bool free_dups, struct rab_getter *getter);
void rab_strip (char *s);
int rab_remove_dups (size_t n_arr, size_t elem_size, void *arr,
		int (*cmp_func) (const void *, const void *),
		void (*free_func) (void *));
int rab_print_joined (int n_objs, void *objs, struct rab_join_opts *opts);
char *rab_join (int n_objs, void *objs, struct rab_join_opts *opts);
struct rab_memstream rab_get_memstream (char *fmt, ...);
struct rab_memstream rab_get_memstream (char *fmt, ...);
struct rab_memstream rab_get_memstream_ap (char *fmt, va_list ap);
int rab_bookend (ssize_t n_pr, char *pr, char *fmt, char *left, char *right);
bool rab_have_just_one_match (int n_matches, size_t n_objs, void *objs, char *arg,
		char *whats, struct rab_getter *getter);
int rab_get_longest_line (char *s);
char **rab_shell_split (char *str, ssize_t *n_out);
bool rab_char_in_str_is_escaped (char *str, int pos);
char *rab_find_unescaped_ch (char *s, char *e, char ch);
char *rab_wordexp_strerror (int rc);
char *rab_get_num_suffix (size_t _num);
char **rab_strsep_str (int *n_r, char *s, char *delims);
int rab_just_write_to_memstream (struct rab_memstream *memstream, char *fmt, ...);
int rab_maybe_close_memstream (struct rab_memstream *memstream, bool free_it);
rab_slice rab_scan_preg (char *s, regex_t preg);
rab_slice rab_scan (char *s, char *pattern);
char *rab_strrchrstart (char *str, char ch);
char *rab_skip_white (char *s);
char* rab_subst (const char* str, const char* from, const char* to, bool g);
